import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Login from './pages/login';
import Home from './pages/home';
import Layout from './components/Layout';
import NotFound from './pages/404';
import NganhNghe from './pages/danh-muc/nganh-nghe/';
import Khoa from './pages/danh-muc/khoa';
import Lop from './pages/danh-muc/lop';
import Chat from './pages/chat/Chat'
import { UserProvider } from './contexts/userContext';
import QuanTriThanhVien from './pages/quan-tri-thanh-vien/';
import { useDispatch } from 'react-redux';
function App() {
  const dispatch = useDispatch()
  const token = localStorage.getItem("token")
  return (
    <UserProvider>
      <BrowserRouter>
        <Routes>
          <Route path='*' element={<NotFound/>}/>
          <Route path="/" element={<Layout/>}>
            <Route path="/" element={<Home/>} />
            <Route path="/quan-tri-thanh-vien" element={<QuanTriThanhVien/>} />
            <Route path='/danh-muc'>
              <Route path='nganh-nghe' element={<NganhNghe/>}></Route>
              <Route path='khoa' element={<Khoa/>}></Route>
              <Route path='lop' element={<Lop/>}></Route>
            </Route>
            <Route path='/chat' element={<Chat/>}/>
          </Route>
          <Route path="/login" element={<Login/>} />
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;

